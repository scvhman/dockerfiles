#!/usr/bin/fish
for folder in (ls -d */ | tr -d "/")
	docker build -t scvh.com/$folder $folder
end
